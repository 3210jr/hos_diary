import { StyleSheet } from 'react-native';


export const primary = '#b71c1c';
export const primary_dark = '#d50000';
export const accent = '#2660f7';


export default styles = StyleSheet.create({
    header: {
        backgroundColor: primary
    }
});