// @ts-check
import firebase from 'react-native-firebase';
import { AppState } from 'react-native';

import {primary_dark} from './styles/main';

// Build a channel
const channel = new firebase.notifications.Android.Channel("reminders", "Reminders Channel", firebase.notifications.Android.Importance.Max)
  .setDescription('Reminders channel');

// Create the channel
firebase.notifications().android.createChannel(channel);


// firebase.notifications().getInitialNotification().then(notificationOpen => {
//     // App was opened by a notification
//     // Get the action triggered by the notification being opened
//     // const action = notification.action;
//     // Get information about the notification that was opened
//     firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId)
// })

firebase.notifications().onNotification((notification) => {
    // Process your notification as required
    console.warn("onNotification")
    const {
        body,
        data,
        title,
        notificationId
    } = notification;
    // if (AppState.currentState === "active") {
        const notif = new firebase.notifications.Notification()
            .setNotificationId(notificationId)
            .setTitle(title)
            .android.setChannelId('reminders')
            .setBody(body)
            .android.setSmallIcon('ic_reminder')
            .android.setLargeIcon('ic_reminder')
            .android.setColor(primary_dark)
            .setSubtitle('This is the subtitile')
            .setData({
                sender: data.sender,
                time: new Date().getTime(),
            });

        firebase.notifications().displayNotification(notif)
        console.warn("Active: ", body, data);
    // }
});


firebase.notifications().onNotificationOpened(notificationOpen => {
    firebase.notifications().removeDeliveredNotification(notificationOpen.notification.notificationId);
})