// @ts-check
import React, { Component } from 'react';
import { Alert, View } from 'react-native';
import { Navigation } from 'react-native-navigation';
import { createStackNavigator } from 'react-navigation';
import { Provider } from 'mobx-react/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import firebase from 'react-native-firebase';

import './Notifications';
import { primary } from './styles/main';

import Entries from './stores/entries';

// Screens
import Home from './views/Home';
import Login from './views/Login';
import ForgotPassword from './views/ForgotPassword';
import NewEntry from './views/NewEntry';
import Help from './views/Help';


const Stack = createStackNavigator(
    {
        Login: Login,
        ForgotPassword: ForgotPassword,
        Home: {
            screen: Home,
            navigationOptions: ({ navigation }) => {
                return {
                    title: 'H.O.S Diary',
                    headerRight: <View style={{ flexDirection: 'row' }}>
                        <Ionicons onPress={() => navigation.navigate("Help")} size={24} style={{ marginRight: 14 }} color="white" name="md-help-circle" />
                        <Ionicons onPress={() => {
                        Alert.alert(
                            'Log out',
                            'Are you sure you want to log out?',
                            [
                                {
                                    text: 'Cancel', style: 'cancel'
                                },
                                {
                                    text: 'Log out', onPress: () => firebase.auth().signOut().then(res => {
                                        navigation.replace('Login')
                                    }).catch(error => {
                                        alert("Error signing out");
                                    })
                                }
                            ]
                        )
                    }} size={24} style={{ marginRight: 10 }} color="white" name="md-exit" /></View>,
                    headerStyle: {
                        backgroundColor: primary,
                        elevation: 0
                    },
                    headerTitleStyle: {
                        color: 'white',
                    },
                }
            }
        },
        NewEntry: NewEntry,
        Help: Help
    },
    {
        initialRouteName: 'Login'
    }
);

export default () => <Provider entries={Entries}><Stack /></Provider>