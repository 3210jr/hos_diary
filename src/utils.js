import firebase from 'react-native-firebase';

export function createNewEntry(title, description, due_date, due_hour, due_min, category, uid) {
    const server_time = firebase.firestore.FieldValue.serverTimestamp;
    const notificationId = uuidv4();
    return firebase.firestore().collection("entries").add({
        title,
        description,
        due_date,
        category,
        completed: false,
        due_hour, due_min,
        uid,
        notificationId,
        created_at: server_time,
        updated_at: server_time
    })
    .then(res => setReminder(notificationId, title, description, due_date, due_hour, due_min))
}

export function setReminder(entry_id, title, description, due_date, due_hour, due_min) {
    const notification = new firebase.notifications.Notification()
        .setNotificationId(entry_id)
        .android.setChannelId('reminders')
        .android.setSmallIcon('ic_reminder')
        .setTitle(title)
        .setBody(description);

    let date = new Date(due_date).setHours(due_hour, due_min);

    return firebase.notifications().scheduleNotification(notification, {
        fireDate: date
    })
}

export function updateEntry(entryId, title, description, due_date, due_hour, due_min, category, uid) {
    const server_time = firebase.firestore.FieldValue.serverTimestamp;
    return firebase.firestore().collection("entries").doc(entryId).update({
        title,
        description,
        due_date,
        due_hour, due_min,
        category,
        updated_at: server_time
    });
}

export function createUserProfile(username, email, uid) {
    const server_time = firebase.firestore.FieldValue.serverTimestamp;
    return firebase.firestore().collection("profiles").doc(uid).set({
        username,
        email,
        uid,
        created_at: server_time,
        updated_at: server_time
    });
}

export function completeEntry(entryId, completed) {
    if (completed) {
        return firebase.firestore().collection("entries").doc(entryId).update({
            completed: false
        });
    }
    return firebase.firestore().collection("entries").doc(entryId).update({
        completed: true
    });
}

export function deleteEntry(entryId) {
    return firebase.firestore().collection("entries").doc(entryId).delete()
}

export function textFormatDate(timeStamp) {
    const dateObj = new Date(timeStamp);

    const date = dateObj.getDate();
    const year = dateObj.getFullYear();

    return `${date} ${getMonthName(dateObj)} ${year}`;
}

export function getMonthName(timeStamp) {
    const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

    return months[new Date(timeStamp).getMonth()]
}

export function shortDate(timeStamp) {
    const dateObj = new Date(timeStamp);
    const date = dateObj.getDate();
    return `${date} ${getMonthName(dateObj)}`;
}

export function uuidv4(a) {
    return a ? (a ^ Math.random() * 16 >> a / 4).toString(16) : ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, uuidv4)
}