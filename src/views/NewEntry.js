// @ts-check
import React, { Component } from 'react';
import {
    View,
    DatePickerAndroid,
    TouchableOpacity,
    Button,
    ToastAndroid,
    TimePickerAndroid,
    StatusBar
} from 'react-native';
import firebase from 'react-native-firebase';
import { Container, Content } from 'native-base';
import { FormInput, FormLabel } from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { createNewEntry, textFormatDate, updateEntry } from '../utils';
import { primary } from '../styles/main';
import Text from '../components/Text';

export default class NewEntry extends Component {
    static navigationOptions = {
        title: 'Create Entry',
        headerStyle: {
            backgroundColor: primary,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            color: 'white'
        }
    };
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            title: "",
            description: "",
            dueDate: new Date(),
            dueTimeHour: 14,
            dueTimeMin: 30
        }
        this.createEntry = this.createEntry.bind(this);
        this.openDatePicker = this.openDatePicker.bind(this);
        this.openTimePicker = this.openTimePicker.bind(this);
        this.updateEntry = this.updateEntry.bind(this);
    }
    async openDatePicker() {
        try {
            const { action, year, month, day } = await DatePickerAndroid.open({
                // Use `new Date()` for current date.
                // May 25 2020. Month 0 is January.
                date: this.state.dueDate
            });
            if (action !== DatePickerAndroid.dismissedAction) {
                // Selected year, month (0-11), day
                this.setState({ dueDate: new Date(year, month, day) })
            }
        } catch ({ code, message }) {
            console.warn('Cannot open date picker', message);
        }
    }
    async openTimePicker() {
        try {
            const {action, hour, minute} = await TimePickerAndroid.open({
              hour: this.state.dueTimeHour,
              minute: this.state.dueTimeMin,
              is24Hour: true, // Will display '2 PM'
            });
            if (action !== TimePickerAndroid.dismissedAction) {
              // Selected hour (0-23), minute (0-59)
              console.warn(hour, minute);
              this.setState({ dueTimeHour: hour, dueTimeMin: minute });
            }
          } catch ({code, message}) {
            console.warn('Cannot open time picker', message);
          }
    }
    updateEntry() {
        const { navigation } = this.props;
        const user = firebase.auth().currentUser;
        const { title, description, dueDate, loading, entry, dueTimeHour, dueTimeMin } = this.state;
        const entryType = navigation.getParam("entryType", "social");
        if (!user) {
            ToastAndroid.show('There is no user signed in.', ToastAndroid.SHORT);
            return;
        }
        if (loading) {
            return;
        }
        if (title.length < 3) {
            ToastAndroid.show('Please enter a valid title.', ToastAndroid.SHORT);
            return;
        }
        this.setState({ loading: true });
        updateEntry(entry.id, title, description, new Date(dueDate).getTime(), dueTimeHour, dueTimeMin, entryType, user.uid).then(res => {
            ToastAndroid.show('Entry updated!', ToastAndroid.SHORT);
            navigation.pop();
        }).catch(error => {
            ToastAndroid.show('Error. Please try again.', ToastAndroid.SHORT);
            this.setState({ loading: false });
        });
    }
    createEntry() {
        const { navigation } = this.props;
        const user = firebase.auth().currentUser;
        const { title, description, dueDate, loading, dueTimeHour, dueTimeMin } = this.state;
        const entryType = navigation.getParam("entryType", "social");
        if (!user) {
            ToastAndroid.show('There is no user signed in.', ToastAndroid.SHORT);
            return;
        }
        if (loading) {
            return;
        }
        if (title.length < 3) {
            ToastAndroid.show('Please enter a valid title.', ToastAndroid.SHORT);
            return;
        }
        this.setState({ loading: true });
        createNewEntry(title, description, new Date(dueDate).getTime(), dueTimeHour, dueTimeMin, entryType, user.uid).then(res => {
            ToastAndroid.show('Entry added!', ToastAndroid.SHORT);
            navigation.pop();
        }).catch(error => {
            ToastAndroid.show('Error. Please try again.', ToastAndroid.SHORT);
            this.setState({ loading: false });
        });
    }
    componentDidMount() {
        const { navigation } = this.props;
        const entryType = navigation.getParam("entryType", "social");
        const edit = navigation.getParam("edit", false);
        if (edit) {
            const entry = navigation.getParam("entry", undefined);

            if (entry) {
                this.setState({ edit, entry, title: entry.title, description: entry.description, dueDate: new Date(entry.due_date), dueTimeHour: entry.due_hour, dueTimeMin: entry.due_min });
            }
        }
    }
    render() {
        const entryType = this.props.navigation.getParam("entryType", "Social");
        return (
            <Container>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={primary}
                />
                <Content>
                    <View style={{ padding: 15, paddingBottom: 0 }}>
                        <Text extraExtraLarge bold>New {entryType} entry</Text>
                    </View>

                    <View>
                        <FormLabel>Title</FormLabel>
                        <FormInput value={this.state.title} onChangeText={text => this.setState({ title: text })} keyboardType="default" />
                    </View>
                    <View>
                        <FormLabel>Description (optional)</FormLabel>
                        <FormInput value={this.state.description} onChangeText={text => this.setState({ description: text })} keyboardType="default" />
                    </View>

                    <View style={{ padding: 15 }}>
                        <TouchableOpacity activeOpacity={0.5} onPress={this.openDatePicker} style={{ borderRadius: 5, borderColor: '#000', borderWidth: 1, padding: 10 }}>
                            <Text bold>Reminder Date:   {textFormatDate(this.state.dueDate)}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ padding: 15 }}>
                        <TouchableOpacity activeOpacity={0.5} onPress={this.openTimePicker} style={{ borderRadius: 5, borderColor: '#000', borderWidth: 1, padding: 10 }}>
                            <Text bold>Reminder Time:   {`${this.state.dueTimeHour} : ${this.state.dueTimeMin}`}</Text>
                        </TouchableOpacity>
                    </View>

                    {
                        this.state.edit ? (
                            <View style={{ padding: 15 }}>
                                <Button color={primary} onPress={this.updateEntry} title={this.state.loading ? "Loading ..." : "Update"} />
                            </View>
                        ) : (
                                <View style={{ padding: 15 }}>
                                    <Button color={primary} onPress={this.createEntry} title={this.state.loading ? "Loading ..." : "Create Entry"} />
                                </View>
                            )
                    }

                </Content>
            </Container>
        )
    }
}