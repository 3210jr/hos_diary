// @ts-check
import React, { Component } from 'react';
import {
    TouchableOpacity,
    View,
    FlatList,
    StatusBar,
    Dimensions
} from 'react-native';
import { Container, Content } from 'native-base';
import { inject, observer } from 'mobx-react/native';
import ActionButton from 'react-native-action-button';
import firebase from 'react-native-firebase';
import Ionicons from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import _ from 'lodash';

import { uuidv4 } from '../utils';
import { primary } from '../styles/main';
import Text from '../components/Text';
import DiaryEntryItem from '../components/DiaryEntryItem';
import EmptyDiaryTab from '../components/EmptyDiaryTab';

const { width, height } = Dimensions.get("window");


@inject("entries")
@observer
export default class HomeTab extends Component {
    constructor(props) {
        super(props);

        this.newEntry = this.newEntry.bind(this);
    }
    newEntry() {
        const { navigation } = this.props;

        navigation.push('NewEntry', { entryType: "home" });
    }
    componentDidMount() {
        // Build notification
        // const notification = new firebase.notifications.Notification()
        //     .setNotificationId(uuidv4())
        //     .android.setChannelId('reminders')
        //     .android.setSmallIcon('ic_reminder')
        //     .setTitle('Reminder!')
        //     .setBody('My notification body')
        //     .setData({
        //         key1: 'value1',
        //         key2: 'value2',
        //     });

        // // Schedule the notification for 1 minute in the future
        // const date = new Date();
        // date.setMinutes(date.getSeconds() + 5);

        // firebase.notifications().scheduleNotification(notification, {
        //     fireDate: date.getTime(),
        // })
        // console.warn("mounted: ", notification)
    }
    render() {
        const { entries, navigation } = this.props;
        return (
            <Container>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={primary}
                />
                <Content>

                    {
                        _.map(entries.homeEntries, entry => {
                            return (
                                <DiaryEntryItem navigation={navigation} entry={entry} key={entry.id} />
                            )
                        })
                    }

                    {
                        entries.homeEntries.length === 0 ? <EmptyDiaryTab /> : <View></View>
                    }

                </Content>

                <ActionButton
                    buttonColor={primary}
                    onPress={this.newEntry}
                />
            </Container>
        )
    }
}


