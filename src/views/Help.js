// @ts-check
import React, { Component } from 'react';
import {
    View,
    DatePickerAndroid,
    TouchableOpacity,
    Button,
    ToastAndroid,
    TimePickerAndroid,
    StatusBar
} from 'react-native';
import { Container, Content } from 'native-base';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { primary } from '../styles/main';
import Text from '../components/Text';

export default class Help extends Component {
    static navigationOptions = {
        title: 'Help',
        headerStyle: {
            backgroundColor: primary,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            color: 'white'
        }
    };
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <Container>
                <Content>
                    <View style={{ padding: 15 }}>
                        <Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit veritatis similique ut quas corporis explicabo, doloribus quos, ratione molestias dignissimos tempore, unde autem a debitis rem repellat cupiditate minus molestiae.
                        </Text>
                        <Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit veritatis similique ut quas corporis explicabo, doloribus quos, ratione molestias dignissimos tempore, unde autem a debitis rem repellat cupiditate minus molestiae.
                        </Text>
                        <Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit veritatis similique ut quas corporis explicabo, doloribus quos, ratione molestias dignissimos tempore, unde autem a debitis rem repellat cupiditate minus molestiae.
                        </Text>
                        <Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit veritatis similique ut quas corporis explicabo, doloribus quos, ratione molestias dignissimos tempore, unde autem a debitis rem repellat cupiditate minus molestiae.
                        </Text>
                        <Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit veritatis similique ut quas corporis explicabo, doloribus quos, ratione molestias dignissimos tempore, unde autem a debitis rem repellat cupiditate minus molestiae.
                        </Text>
                        <Text>
                            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Reprehenderit veritatis similique ut quas corporis explicabo, doloribus quos, ratione molestias dignissimos tempore, unde autem a debitis rem repellat cupiditate minus molestiae.
                        </Text>
                    </View>

                </Content>
            </Container>
        )
    }
}