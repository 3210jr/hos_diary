// @ts-check
import React, { Component } from 'react';
import { View } from 'react-native';
import { inject, observer } from 'mobx-react/native';
import { Container, Content } from 'native-base';
import ActionButton from 'react-native-action-button';
import _ from 'lodash';

import { primary } from '../styles/main';
import Text from '../components/Text';

import DiaryEntryItem from '../components/DiaryEntryItem';
import EmptyDiaryTab from '../components/EmptyDiaryTab';

@inject("entries")
@observer
export default class OfficeTab extends Component {
    constructor(props) {
        super(props);

        this.newEntry = this.newEntry.bind(this);
    }
    newEntry() {
        const { navigation } = this.props;

        navigation.push('NewEntry', { entryType: "office" });
    }
    render() {
        const { entries, navigation } = this.props;
        return (
            <Container>
                <Content>
                    {
                        _.map(entries.officeEntries, entry => {
                            return (
                                <DiaryEntryItem navigation={navigation} entry={entry} key={entry.id} />
                            )
                        })
                    }

                    {
                        entries.officeEntries.length === 0 ? <EmptyDiaryTab />:<View></View>
                    }
                </Content>


                <ActionButton
                    buttonColor={primary}
                    onPress={this.newEntry}
                />
            </Container>
        )
    }
}