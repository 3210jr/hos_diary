// @ts-check
import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Content } from 'native-base';
import { TabNavigator } from 'react-navigation';
import { Provider } from 'mobx-react/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

import { primary } from '../styles/main';
import Text from '../components/Text';

import HomeTab from './HomeTab';
import SocialTab from './SocialTab';
import OfficeTab from './OfficeTab';

import Entries from '../stores/entries';


export default TabNavigator(
    {
        Home: HomeTab,
        Office: OfficeTab,
        Social: SocialTab
    },
    {
        navigationOptions: ({ navigation }) => ({
            tabBarIcon: ({ focused, tintColor }) => {
                const { routeName } = navigation.state;
                let iconName;
                if (routeName === 'Home') {
                    iconName = `ios-information-circle${focused ? '' : '-outline'}`;
                } else if (routeName === 'Settings') {
                    iconName = `ios-options${focused ? '' : '-outline'}`;
                }
                // You can return any component that you like here! We usually use an
                // icon component from react-native-vector-icons
                return <Ionicons name={iconName} size={25} color={tintColor} />;
            },
        }),
        tabBarOptions: {
            activeTintColor: 'white',
            inactiveTintColor: '#cece',
            style: {
                backgroundColor: primary
            },
            indicatorStyle: {
                backgroundColor: 'white'
            },
        }
    }
)