// @ts-check
import React, { Component } from 'react';
import {
    View,
    Text,
    Button,
    Image,
    TouchableOpacity,
    Dimensions,
    StatusBar
} from 'react-native';
import firebase from 'react-native-firebase';
import { Container, Content } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons';
import { FormInput, FormLabel } from 'react-native-elements';

import { primary, primary_dark } from '../styles/main';
import { createUserProfile } from '../utils';

// @ts-ignore
const logo = require('../assets/logo.png');
const { width, height } = Dimensions.get("window");


export default class Login extends Component {
    static navigationOptions = {
        header: null
    };
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            activeView: 'signIn',
            username: "",
            email: "",
            password: ""
        }

        this.signIn = this.signIn.bind(this);
        this.signUp = this.signUp.bind(this);
        this.toggleActiveView = this.toggleActiveView.bind(this);
        this.goToResetPassword = this.goToResetPassword.bind(this);
    }
    goToResetPassword() {
        const { navigation } = this.props;
        navigation.navigate('ForgotPassword');
    }
    toggleActiveView() {
        const activeView = this.state.activeView === "signIn" ? "signUp" : "signIn";
        this.setState({ activeView });
    }
    signIn() {
        const { navigation } = this.props;
        const { email, password, loading } = this.state;
        if (loading) {
            return;
        }
        if (email.length < 5) {
            alert("Please enter a valid email");
            return;
        }

        if (password.length < 3) {
            alert("Your password is too short.");
            return;
        }

        this.setState({ loading: true });
        firebase.auth().signInAndRetrieveDataWithEmailAndPassword(email, password).then(res => {
            navigation.replace('Home')
        }).catch(error => {
            alert("There was an error logging in, please try again");
            this.setState({ loading: false });
        });
    }
    signUp() {
        const { navigation } = this.props;
        const { email, password, username, loading } = this.state;
        if (loading) {
            return;
        }

        if (email.length < 5) {
            alert("Please enter a valid email");
            return;
        }

        if (password.length < 3) {
            alert("Your password is too short.");
            return;
        }

        this.setState({ loading: true });
        firebase.auth().createUserAndRetrieveDataWithEmailAndPassword(email, password).then(UserCredential => {
            createUserProfile(username, email, UserCredential.user.uid);
            navigation.replace('Home')
        }).catch(error => {
            alert("Error creating a new user, please try again.");
            this.setState({ loading: false });
        });
    }
    componentDidMount() {
        const { navigation } = this.props;
        this.authListener = firebase.auth().onAuthStateChanged(user => {
            if (user) {
                navigation.replace('Home');
            } else {
                this.setState({ loading: false });
            }
        });
    }
    componentWillUnmount() {
        if (this.authListener) {
            this.authListener();
        }
    }
    render() {
        return (
            <Container>
                <StatusBar
                    barStyle="light-content"
                    backgroundColor={primary}
                />
                <Content>
                    <View style={{ paddingTop: height / 8, paddingBottom: 20 }}>
                        <Image resizeMode="contain" style={{ width: width / 3, height: width / 3, alignSelf: 'center' }} source={logo} />
                    </View>

                    {
                        this.state.activeView === "signUp" ? (
                            <View>
                                <FormLabel>Username</FormLabel>
                                <FormInput autoCapitalize="none" value={this.state.username} onChangeText={text => this.setState({ username: text })} keyboardType="email-address" />
                            </View>
                        ) : (
                                <View></View>
                            )
                    }


                    <View>
                        <FormLabel>Email</FormLabel>
                        <FormInput autoCapitalize="none" value={this.state.email} onChangeText={text => this.setState({ email: text })} keyboardType="email-address" />
                    </View>

                    <View>
                        <FormLabel>Password</FormLabel>
                        <FormInput autoCapitalize="none" value={this.state.password} onChangeText={text => this.setState({ password: text })} secureTextEntry={true} />
                    </View>

                    {
                        this.state.activeView === "signIn" ? (
                            <View>
                                <TouchableOpacity onPress={this.goToResetPassword} activeOpacity={0.5} style={{ padding: 15 }}>
                                    <Text>Forgot password?</Text>
                                </TouchableOpacity>
                                <View style={{ padding: 15 }}>
                                    <Button onPress={this.signIn} color={primary} title={this.state.loading ? "Loading ...":"Sign In"} />
                                </View>
                                <TouchableOpacity onPress={this.toggleActiveView} activeOpacity={0.5} style={{ padding: 15, paddingTop: 0 }}>
                                    <Text style={{ alignSelf: 'flex-end' }}>Or sign up instead.</Text>
                                </TouchableOpacity>
                            </View>
                        ) : (
                                <View>
                                    <View style={{ padding: 15 }}>
                                        <Button onPress={this.signUp} color={primary} title={this.state.loading ? "Loading ...":"Sign Up"} />
                                    </View>
                                    <TouchableOpacity onPress={this.toggleActiveView} activeOpacity={0.5} style={{ padding: 15, paddingTop: 0 }}>
                                        <Text style={{ alignSelf: 'flex-end' }}>Or sign in instead.</Text>
                                    </TouchableOpacity>
                                </View>
                            )
                    }


                </Content>
            </Container>
        )
    }
}