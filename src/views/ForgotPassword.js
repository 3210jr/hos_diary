// @ts-check
import React, { Component } from 'react';
import {
    View,
    Dimensions,
    Button,
    ToastAndroid
} from 'react-native';
import firebase from 'react-native-firebase';
import { Container, Content } from 'native-base';
import { FormInput, FormLabel } from 'react-native-elements';

import { primary } from '../styles/main';
import Text from '../components/Text';

export default class ForgotPassword extends Component {
    static navigationOptions = {
        title: 'Password Reset',
        headerStyle: {
            backgroundColor: primary,
        },
        headerTintColor: 'white',
        headerTitleStyle: {
            color: 'white'
        }
    };
    constructor(props) {
        super(props);

        this.state = {
            email: "",
            loading: false
        };

        this.sendResetLink = this.sendResetLink.bind(this);
    }
    sendResetLink() {
        const { email, loading } = this.state;
        if (loading) {
            ToastAndroid.show("Please wait while loading.", ToastAndroid.SHORT);
            return;
        }
        this.setState({ loading: true });
        firebase.auth().sendPasswordResetEmail(email).then(res => {
            ToastAndroid.show("Reset link sent to your email.", ToastAndroid.SHORT);
            this.props.navigation.pop();
        }).catch(error => {
            ToastAndroid.show("Error sending reset link.", ToastAndroid.SHORT);
            this.setState({ loading: false });
        })
    }
    render() {
        return (
            <Container>
                <Content>
                    <View style={{ padding: 15 }}>
                        <Text large>
                            You will receive an email with the password reset instructions.
                        </Text>
                    </View>

                    <View>
                        <FormLabel>Your email</FormLabel>
                        <FormInput keyboardType="email-address" value={this.state.email} onChangeText={text => this.setState({ email: text })} />
                    </View>


                    <View style={{ padding: 15 }}>
                        <Button onPress={this.sendResetLink} title={this.state.loading ? "Loading ..." : "Reset password"} />
                    </View>
                </Content>
            </Container>
        )
    }
}