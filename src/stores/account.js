// @ts-check
import firebase from 'react-native-firebase';
import {
    observable,
    computed,
    action
} from 'mobx';
import {
    Alert,
    AsyncStorage
} from 'react-native';
import _ from 'lodash';
import uuidv1 from 'uuid/v1';


class Account {
    @observable _user = undefined;
    @observable _loadingUser = true;

    @observable _file = undefined;
    @observable _loadingFile = true;

    @observable _checkpoints = undefined;
    @observable _loadingCheckpoints = true;


    constructor() {

    }

    @computed get user() {
        return this._user;
    }

    @computed get loadingUser() {
        return this._loadingUser;
    }

    @computed get file() {
        return this._file;
    }

    @computed get loadingFile() {
        return this._loadingFile;
    }

    @computed get checkpoints() {
        return this._checkpoints;
    }

    @computed get loadingCheckpoints() {
        return this._loadingCheckpoints;
    }
}


export default new Account()