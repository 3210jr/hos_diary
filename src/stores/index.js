import Account from './account';
import Entries from './entries';

export default {
    account: Account,
    entries: Entries
};