// @ts-check
import firebase from 'react-native-firebase';
import {
    observable,
    computed,
    action
} from 'mobx';
import {
    Alert,
    AsyncStorage
} from 'react-native';
import _ from 'lodash';


class Entries {
    @observable _entries = [];
    @observable _loadingEntries = true;


    constructor() {
        firebase.auth().onAuthStateChanged(user => {
            if (user) {
                firebase.firestore().collection("entries").where("uid", "==", user.uid).onSnapshot(snap => {
                    const entries = [];
                    snap.forEach(child => {
                        const entry = child.data();
                        entry["id"] = child.id;
                        entries.push(entry);
                    });
                    this._entries = _.sortBy(entries, ['due_date']);
                });
            }
        })
    }

    @computed get entries() {
        return this._entries;
    }

    @computed get homeEntries() {
        return _.filter(this._entries, ['category', "home"]);
    }

    @computed get officeEntries() {
        return _.filter(this._entries, ['category', "office"]);
    }

    @computed get socialEntries() {
        return _.filter(this._entries, ['category', "social"]);
    }

    @computed get loadingEntries() {
        return this._loadingEntries;
    }
}


export default new Entries()