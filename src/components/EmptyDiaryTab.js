// @ts-check
import React, { Component } from 'react';
import {
    View,
    Dimensions
} from 'react-native';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import { primary } from '../styles/main';
import Text from '../components/Text';

const { width, height } = Dimensions.get("window");


export default function EmptyDiaryTab () {
    return (
        <View style={{ flex: 1, justifyContent: 'center', paddingTop: height / 4, width, alignItems: 'center' }}>
            <MaterialCommunityIcons size={82} name="beach" />
            <View style={{ height: 25 }} />
            <Text extraExtraLarge>This list is empty!</Text>
        </View>
    )
}