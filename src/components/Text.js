// @ts-check
import React from 'react';
import {
    Text,
    StyleSheet
} from 'react-native';

export default ({ 
    children = "", 
    numberOfLines = undefined,
    bold = false, 
    white = false, 
    large = false, 
    extraLarge = false, 
    extraExtraLarge = false, 
    extremelyLarge = false, 
    textAlign = 'left', 
    style = {} }) => (
    <Text numberOfLines={numberOfLines} style={
        [
            styles.default,
            bold ? styles.boldText : styles.text,
            white && styles.whiteText,
            large && styles.largeText,
            extraLarge && styles.extraLarge,
            extraExtraLarge && styles.extraExtraLarge,
            extremelyLarge && styles.extremelyLarge,
            { textAlign: textAlign },
            style
        ]
    }>
        {children}
    </Text>
)

const styles = StyleSheet.create({
    default: {
        fontSize: 16
    },
    whiteText: {
        color: 'white'
    },
    text: {
        fontFamily: 'JosefinSans-Regular'
    },
    boldText: {
        fontFamily: 'JosefinSans-Bold'
    },
    largeText: {
        fontSize: 17
    },
    extraLarge: {
        fontSize: 19
    },
    extraExtraLarge: {
        fontSize: 22
    },
    extremelyLarge: {
        fontSize: 30
    }
})