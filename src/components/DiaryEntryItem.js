// @ts-check
import React, { Component } from 'react';
import {
    TouchableOpacity,
    View,
    FlatList,
    Alert,
    ToastAndroid
} from 'react-native';

import { completeEntry, deleteEntry, shortDate } from '../utils';
import Text from './Text';

export default function DiaryEntryItem({ entry, navigation }) {
    function showOptions() {
        Alert.alert(
            'Options',
            'What do you want to do with this entry?',
            [
                {
                    text: 'Delete', style: 'cancel', onPress: () => {
                        deleteEntry(entry.id).then(res => {
                            ToastAndroid.show('Deleted!', ToastAndroid.SHORT);
                        }).catch(error => {
                            ToastAndroid.show('Error. Try again.', ToastAndroid.SHORT);
                        })
                    }
                },
                {
                    text: 'Edit', onPress: () => {
                        navigation.navigate('NewEntry', { edit: true, entry, entryType: entry.category });
                    }
                },
                {
                    text: entry.completed ? 'Uncomplete' : 'Complete', onPress: () => {
                        completeEntry(entry.id, entry.completed).then(res => {
                            ToastAndroid.show('Completed!', ToastAndroid.SHORT);
                        }).catch(error => {
                            ToastAndroid.show('Error. Try again.', ToastAndroid.SHORT);
                        })
                    }
                }
            ]
        )
    }
    return (
        <TouchableOpacity activeOpacity={0.5} onPress={showOptions} style={[{ flexDirection: 'row', paddingBottom: 15, borderBottomWidth: 1, borderBottomColor: '#ccc', padding: 10 }, entry.completed && { borderLeftColor: 'green', borderLeftWidth: 5, backgroundColor: '#ccc' }]}>
            <View style={{ flex: 3 }}>
                <Text extraLarge bold>{entry.title}</Text>
                <Text large>{entry.description}</Text>
            </View>
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'flex-end', paddingRight: 15 }}>
                <Text>{shortDate(entry.due_date)}</Text>
            </View>
        </TouchableOpacity>
    )
}