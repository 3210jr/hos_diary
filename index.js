import { AppRegistry } from 'react-native';
import MainApp from './src/MainApp';

AppRegistry.registerComponent('hos_diary', () => MainApp);
